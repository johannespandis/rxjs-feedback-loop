const tsConfig = require('./samples/tsconfig.json');
const tsConfigPaths = require('tsconfig-paths');

const baseUrl = './'
const cleanup = tsConfigPaths.register({
    baseUrl,
    paths: tsConfig.compilerOptions.paths,
});

