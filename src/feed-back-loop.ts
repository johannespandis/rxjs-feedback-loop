import { Observable, OperatorFunction, Observer, throwError, ObservedValueOf, NEVER, ReplaySubject, Subject, asapScheduler, of, Subscriber } from 'rxjs'
import { tap, takeUntil, catchError, map, filter } from 'rxjs/operators';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

export interface ISideChannel<ResType> {
    out<InType>(extract: (inVal: InType) => ResType): OperatorFunction<InType, InType>;
    outIf<InType>(pred: (inVal: InType) => boolean, extract: (inVal: InType) => ResType): OperatorFunction<InType, InType>;
    completeIf<InType>(pred: (inVal: InType) => boolean): OperatorFunction<InType, InType | never>;
    errorIf<InType>(pred: (inVal: InType) => boolean, toError: (inVal: InType) => any): OperatorFunction<InType, InType>;
    onError<InType>(complete: (err: any) => boolean): OperatorFunction<InType, InType | ObservedValueOf<Observable<never>>>;
    transformError<InType>(transform: (err: any) => ResType): OperatorFunction<InType, InType | ResType>
}

class SideChannel<ResType> implements ISideChannel<ResType> {
    constructor(private observer: Observer<ResType>) {}
    
    out<InType>(extract: (inVal: InType) => ResType): OperatorFunction<InType, InType> {
        return (inObs: Observable<InType>) => inObs.pipe(tap(inVal => this.observer.next(extract(inVal))))
    }
    
    outIf<InType>(pred: (inVal: InType) => boolean, extract: (inVal: InType) => ResType): OperatorFunction<InType, InType> {
        return (inObs: Observable<InType>) => inObs.pipe(
            tap(inVal => {
                if (pred(inVal)) {
                    this.observer.next(extract(inVal));
                }
            })
        );
    }
    
    completeIf<InType>(pred: (inVal: InType) => boolean): OperatorFunction<InType, InType> {
        return (inObs: Observable<InType>) => inObs.pipe(
            map(inVal => [inVal, pred(inVal)] as [InType, Boolean]),
            tap(([_, isComplete]) => {
                if (isComplete) {
                    this.observer.complete();
                }
            }),
            filter(([_, isComplete]) => !isComplete),
            map(([inVal]) => inVal)
        )
    }
    
    errorIf<InType>(pred: (inVal: InType) => boolean, toError: (inVal: InType) => any): OperatorFunction<InType, InType> {
        return (inObs: Observable<InType>) => inObs.pipe(tap(inVal => {
            if (pred(inVal)) {
                this.observer.error(toError(inVal))
            }
        }));
    }

    onError<InType>(complete: (err: any) => boolean) {  
        return (inObs: Observable<InType>) => inObs.pipe(
            catchError(err => {
                this.observer.error(err);
                if(complete(err)) {
                    this.observer.complete();
                    return NEVER;
                } else {
                    return throwError(err);
                }
            })
        )
    }

    transformError<InType>(transform: (err:any) => ResType) {
        return (inObs: Observable<InType>) => inObs.pipe(
            catchError(err => {
                const transformed = transform(err);
                this.observer.next(transformed)
                return of(transformed)
            })
        )
    }
}

class StateQueue<StateType, ResultType> implements Observer<StateType> {
    states: StateType[] = [];
    currentState?: StateType

    constructor(
        private feedSubject: Subject<StateType>,
        private finishSubject: ReplaySubject<void>,
        private subscriber: Subscriber<ResultType>) {}

    next(nextState: StateType) {
        if (this.currentState != null) {
            this.states.push(nextState);
        } else {
            this.currentState = nextState;
            while (this.currentState != null) {
                this.feedSubject.next(this.currentState);
                this.currentState = this.states.shift();
            }
        }
    }

    complete() {
        this.states = [];
    }

    error(err: any) {
        this.finishSubject.next();
        this.subscriber.error(err);
    }
}

export function feedBackLoop<StateType, ResType>(
        iteration: (feedObs: Observable<StateType>, sideChannel: ISideChannel<ResType>) => Observable<StateType>, 
        initialState: StateType
    ): Observable<ResType> {
    
   return new Observable(subscriber => {
        const finishSubject = new ReplaySubject<void>(1);
        const feedObs = new Subject<StateType>();
        const stateQueue = new StateQueue(feedObs, finishSubject, subscriber);
        const sideChannel = new SideChannel<ResType>({
            next: res => subscriber.next(res),
            complete: () => {
                finishSubject.next();
                subscriber.complete();
            },
            error: err => subscriber.error(err),
        });

        iteration(feedObs, sideChannel).pipe(
            takeUntil(finishSubject),
        ).subscribe(stateQueue);

        feedObs.next(initialState);

        return () => finishSubject.next();
   })     


}