"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _lib_1 = require("@lib");
var operators_1 = require("rxjs/operators");
var obs = _lib_1.feedBackLoop(function (feedObs, sideChannel) {
    return feedObs.pipe(sideChannel.out(function (n) { return n; }), operators_1.map(function (n) { return n + 1; }), sideChannel.completeIf(function (n) { return n > 10; }), operators_1.delay(1000));
}, 0);
obs.subscribe(function (n) { return console.log(n); });
