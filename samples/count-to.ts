import { feedBackLoop } from '@lib';
import { map, delay } from 'rxjs/operators';



const obs = feedBackLoop((feedObs, sideChannel) => 
    feedObs.pipe(
        sideChannel.out(n => n),
        map(n => n + 1),
        sideChannel.completeIf(n => n > 10),
        delay(1000)
    )
, 0)

obs.subscribe(n => console.log(n))
